﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormAddField : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public int index = -1;
        public FormAddField()
        {
            InitializeComponent();
        }

        public int[] book;

        private void FormAddField_Load(object sender, EventArgs e)
        {
            string nameBooks = "SELECT * FROM BookTable";
            string nameField = "SELECT * FROM FieldOfKnowledgeTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(nameBooks, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable fieldTable = ds.Tables.Add("Field");
                adapter = new SqlDataAdapter(nameField, connection);
                adapter.Fill(ds, "Field");
                book = new int[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    book[i] = Convert.ToInt32(ds.Tables[0].Rows[i][0]);
                    cbNameBooks.Items.Add(ds.Tables[0].Rows[i][1].ToString());
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    cbField.Items.Add(ds.Tables[1].Rows[i][1].ToString());
                }
            }

            if (this.Text == "Редактирование")
            {
                var queryEdit = "SELECT * FROM sBookField WHERE BookFieldId = " + index;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(queryEdit, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    cbNameBooks.SelectedItem = ds.Tables[0].Rows[0][2].ToString();
                    cbField.SelectedItem = ds.Tables[0].Rows[0][3].ToString();
                }
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(cbField.Text))
            {
                var field = "SELECT FieldOfKnowledgeId FROM FieldOfKnowledgeTable WHERE NameFieldOfKnowledge = '" + cbField.Text + "'";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(field, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        var queryField = "INSERT INTO FieldOfKnowledgeTable(NameFieldOfKnowledge) " +
                        "VALUES (@NameFieldOfKnowledge)";
                        SqlCommand command = new SqlCommand(queryField, connection);
                        SqlParameter fieldParameter = new SqlParameter("@NameFieldOfKnowledge", cbField.Text);
                        command.Parameters.Add(fieldParameter);
                        command.ExecuteNonQuery();
                    }
                }

                if (this.Text == "Добавление")
                {
                    var query = "INSERT INTO BookWithFieldOfKnowledge(BookId, FieldOfKnowledgeId) " +
                        "VALUES ((SELECT BookId FROM BookTable WHERE BookName = @BookName), " +
                        "(SELECT FieldOfKnowledgeId FROM FieldOfKnowledgeTable WHERE NameFieldOfKnowledge = @NameFieldOfKnowledge))";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        SqlParameter bookParameter = new SqlParameter("@BookName", cbNameBooks.Text);
                        command.Parameters.Add(bookParameter);
                        SqlParameter fieldParameter = new SqlParameter("@NameFieldOfKnowledge", cbField.Text);
                        command.Parameters.Add(fieldParameter);
                        command.ExecuteNonQuery();
                    }
                    MessageBox.Show("Данные добавлены в таблицу", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    var query = "UPDATE BookWithFieldOfKnowledge " +
                        "SET BookId = " + book[cbNameBooks.SelectedIndex] + ", FieldOfKnowledgeId = "
                        + "(SELECT FieldOfKnowledgeId FROM FieldOfKnowledgeTable WHERE NameFieldOfKnowledge = '" + cbField.Text + "') " +
                        "WHERE BookFieldId = " + index;
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                    }
                    MessageBox.Show("Данные изменены", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                this.Close();
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}