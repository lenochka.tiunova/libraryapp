﻿
namespace LibraryApp
{
    partial class FormRemove
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblList = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.dgvBookRemove = new System.Windows.Forms.DataGridView();
            this.lblCheckNull = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookRemove)).BeginInit();
            this.SuspendLayout();
            // 
            // lblList
            // 
            this.lblList.AutoSize = true;
            this.lblList.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblList.ForeColor = System.Drawing.Color.CornflowerBlue;
            this.lblList.Location = new System.Drawing.Point(204, 25);
            this.lblList.Name = "lblList";
            this.lblList.Size = new System.Drawing.Size(365, 32);
            this.lblList.TabIndex = 0;
            this.lblList.Text = "Список \r\nна исключение объектов библиотечного фонда";
            this.lblList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnOk
            // 
            this.btnOk.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnOk.FlatAppearance.BorderSize = 0;
            this.btnOk.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnOk.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnOk.Location = new System.Drawing.Point(207, 389);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(75, 23);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "Утвердить";
            this.btnOk.UseVisualStyleBackColor = false;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnCancel.FlatAppearance.BorderSize = 0;
            this.btnCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCancel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnCancel.Location = new System.Drawing.Point(494, 389);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Отклонить";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dgvBookRemove
            // 
            this.dgvBookRemove.BackgroundColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.dgvBookRemove.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvBookRemove.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBookRemove.Location = new System.Drawing.Point(29, 80);
            this.dgvBookRemove.Name = "dgvBookRemove";
            this.dgvBookRemove.Size = new System.Drawing.Size(746, 289);
            this.dgvBookRemove.TabIndex = 3;
            // 
            // lblCheckNull
            // 
            this.lblCheckNull.AutoSize = true;
            this.lblCheckNull.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblCheckNull.ForeColor = System.Drawing.Color.Red;
            this.lblCheckNull.Location = new System.Drawing.Point(338, 61);
            this.lblCheckNull.Name = "lblCheckNull";
            this.lblCheckNull.Size = new System.Drawing.Size(0, 16);
            this.lblCheckNull.TabIndex = 4;
            this.lblCheckNull.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormRemove
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.lblCheckNull);
            this.Controls.Add(this.dgvBookRemove);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.lblList);
            this.Name = "FormRemove";
            this.Text = "Акт списания";
            this.Load += new System.EventHandler(this.FormRemove_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvBookRemove)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblList;
        private System.Windows.Forms.DataGridView dgvBookRemove;
        private System.Windows.Forms.Label lblCheckNull;
        public System.Windows.Forms.Button btnOk;
        public System.Windows.Forms.Button btnCancel;
    }
}