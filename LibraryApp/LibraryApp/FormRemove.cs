﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormRemove : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormRemove()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            var sql = "SELECT * FROM BookActTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                List<int> bookDelete = new List<int>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    bookDelete.Add(int.Parse(ds.Tables[0].Rows[i][0].ToString()));
                }

                if (bookDelete.Count != 0)
                {
                    var query = "DELETE FROM BookTable WHERE BookId = " + bookDelete[0];

                    if (bookDelete.Count > 1)
                    {
                        for (int i = 1; i < bookDelete.Count; i++)
                        {
                            query += " OR BookId = " + bookDelete[i];
                        }
                    }

                    SqlCommand sqlCommand = new SqlCommand(query, connection);
                    sqlCommand.ExecuteNonQuery();

                    sql = "DELETE FROM dbo.BookActTable";
                    sqlCommand = new SqlCommand(sql, connection);
                    sqlCommand.ExecuteNonQuery();
                }
            }
            MessageBox.Show("Книги списаны", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.DialogResult = DialogResult.OK;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            var sql = "DELETE FROM dbo.BookActTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlCommand sqlCommand = new SqlCommand(sql, connection);
                sqlCommand.ExecuteNonQuery();
            }
            MessageBox.Show("Акт отклонен", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            dgvView();
        }

        private void dgvView()
        {
            var sql = "SELECT * FROM BookActTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);

                List<int> bookDelete = new List<int>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    bookDelete.Add(int.Parse(ds.Tables[0].Rows[i][0].ToString()));
                }

                if (bookDelete.Count != 0)
                {
                    var query = "SELECT * FROM sBookRemove WHERE BookId = " + bookDelete[0];

                    if (bookDelete.Count > 1)
                    {
                        for (int i = 1; i < bookDelete.Count; i++)
                        {
                            query += " OR BookId = " + bookDelete[i];
                        }
                    }

                    adapter = new SqlDataAdapter(query, connection);
                    ds = new DataSet();
                    adapter.Fill(ds);
                    dgvBookRemove.DataSource = ds.Tables[0];
                    dgvBookRemove.Columns[0].HeaderText = "Номер книги";
                    dgvBookRemove.Columns[1].HeaderText = "Название книги";
                    dgvBookRemove.Columns[2].HeaderText = "Количество экземпляров";
                    dgvBookRemove.Columns[3].HeaderText = "Стоимость";
                    dgvBookRemove.Columns[4].HeaderText = "Итого";
                    dgvBookRemove.AutoResizeColumns();
                    dgvBookRemove.AutoResizeRows();
                }
                else
                {
                    dgvBookRemove.DataSource = null;
                    btnCancel.Enabled = false;
                    btnOk.Enabled = false;
                    lblCheckNull.Text = "Список пуст";
                }
            }
        }

        private void FormRemove_Load(object sender, EventArgs e)
        {
            dgvView();
        }
    }
}
