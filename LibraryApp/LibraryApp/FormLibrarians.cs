﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormLibrarians : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormLibrarians()
        {
            InitializeComponent();
        }

        private void dgvAdd(string sql)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvLibrarians.DataSource = ds.Tables[0];
            }
            dgvLibrarians.AutoResizeColumns();
            dgvLibrarians.AutoResizeRows();
        }

        public void block()
        {
            miAddBook.Visible = false;
            miDelete.Visible = false;
            miClose.Visible = false;
            miBookField.Visible = false;
            miEditField.Visible = false;
            miCopyLocation.Visible = false;
            miGive.Visible = false;
            miAccept.Visible = false;
            gbLiner.Visible = false;
        }

        private void miBook_Click(object sender, EventArgs e)
        {
            block();
            miAddBook.Visible = true;
            miDelete.Visible = true;
            string sql = "SELECT * FROM sBooksLibr";
            dgvAdd(sql);
            dgvLibrarians.Columns[0].HeaderText = "Номер книги";
            dgvLibrarians.Columns[1].HeaderText = "Название книги";
            dgvLibrarians.Columns[2].HeaderText = "Область знаний";
            dgvLibrarians.Columns[3].HeaderText = "Издательство";
            dgvLibrarians.Columns[4].HeaderText = "Город";
            dgvLibrarians.Columns[5].HeaderText = "Год издания";
            dgvLibrarians.Columns[6].HeaderText = "Количество страниц";
            dgvLibrarians.Columns[7].HeaderText = "Автор";
            dgvLibrarians.Columns[8].HeaderText = "  ";
            dgvLibrarians.Columns[9].HeaderText = " ";
            dgvLibrarians.Columns[10].HeaderText = "Стоимость";
            dgvLibrarians.Columns[10].DefaultCellStyle.Format = "N2";
            dgvLibrarians.AutoResizeColumns();
            dgvLibrarians.AutoResizeRows();
        }

        private void miExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
        }

        private void miBookFieldOfKnowledge_Click(object sender, EventArgs e)
        {
            block();
            miBookField.Visible = true;
            miEditField.Visible = true;
            string sql = "SELECT * FROM sBookField";
            dgvAdd(sql);
            dgvLibrarians.Columns[0].Visible = false;
            dgvLibrarians.Columns[1].HeaderText = "Номер книги";
        }

        private void miCopy_Click(object sender, EventArgs e)
        {
            block();
            miCopyLocation.Visible = true;
            string sql = "SELECT * FROM sBookCopy";
            dgvAdd(sql);
            dgvLibrarians.Columns[0].Visible = false;
        }

        private void miReaders_Click(object sender, EventArgs e)
        {
            block();
            miClose.Visible = true;
            string sql = "SELECT * FROM sReader";
            dgvAdd(sql);
            dgvLibrarians.Columns[0].Visible = false;
        }

        private void FormLibrarians_Load(object sender, EventArgs e)
        {
            var sql = "SELECT DISTINCT LibraryCardId FROM sLiner";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                cbLiner.Items.Add("Все");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    cbLiner.Items.Add(ds.Tables[0].Rows[i][0]);
                }
            }
            miBook_Click(null,null);
        }

        private void FormLibrarians_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void dgvLibrarians_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (DBNull.Value == e.Value)
            {
                e.Value = "-";
            }
        }

        private void miHistory_Click(object sender, EventArgs e)
        {
            block();
            miGive.Visible = true;
            miAccept.Visible = true;
            gbLiner.Visible = true;
            string sql = "SELECT * FROM sLiner";
            dgvAdd(sql);
            LinerHeader();
        }

        public void LinerHeader()
        {
            dgvLibrarians.Columns[0].Visible = false;
            dgvLibrarians.Columns[1].HeaderText = "Читательский билет №";
            dgvLibrarians.Columns[2].HeaderText = "Читатель";
            dgvLibrarians.Columns[3].HeaderText = " ";
            dgvLibrarians.Columns[4].HeaderText = " ";
            dgvLibrarians.Columns[5].HeaderText = "Номер книги";
            dgvLibrarians.Columns[6].HeaderText = "Название";
            dgvLibrarians.Columns[7].HeaderText = "Дата выдачи";
            dgvLibrarians.Columns[8].HeaderText = "Дата возврата";
        }

        private void cbLiner_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM sLiner";
            if (cbLiner.SelectedIndex != -1 && cbLiner.SelectedIndex != 0)
            {
                sql += " WHERE LibraryCardId = " + cbLiner.SelectedItem;
            }
            dgvAdd(sql);
            LinerHeader();
        }

        private void miGive_Click(object sender, EventArgs e)
        {
            FormGiveBook giveBook = new FormGiveBook();
            giveBook.ShowDialog();
            miHistory_Click(null, null);
        }

        private void miAccept_Click(object sender, EventArgs e)
        {
            DataSet ds = new DataSet();
            var sql = "SELECT * FROM LinerTable WHERE DateReturn IS NULL AND LinerId = " + dgvLibrarians.Rows[dgvLibrarians.CurrentRow.Index].Cells[0].Value;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                adapter.Fill(ds);
            }

            if (dgvLibrarians.CurrentRow != null && ds.Tables[0].Rows.Count != 0)
            {
                var query = "UPDATE LinerTable " +
                            "SET DateReturn = '" + DateTime.Now.ToShortDateString() + "' " +
                            "WHERE LinerId = " + dgvLibrarians.Rows[dgvLibrarians.CurrentRow.Index].Cells[0].Value;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();
                }
                MessageBox.Show("Данные изменены", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                miHistory_Click(null, null);
            }
            else
            {
                MessageBox.Show("Выберите книгу, которую еще не вернули", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miBookField_Click(object sender, EventArgs e)
        {
            FormAddField addField = new FormAddField();
            addField.Text = "Добавление";
            addField.ShowDialog();
            miBookFieldOfKnowledge_Click(null, null);
        }

        private void miEditField_Click(object sender, EventArgs e)
        {
            FormAddField addField = new FormAddField();
            addField.Text = "Редактирование";
            addField.index = Convert.ToInt32(dgvLibrarians.Rows[dgvLibrarians.CurrentRow.Index].Cells[0].Value);
            addField.ShowDialog();
            miBookFieldOfKnowledge_Click(null, null);
        }

        private void miCopyLocation_Click(object sender, EventArgs e)
        {
            FormCopyLocation copyLocation = new FormCopyLocation();
            copyLocation.ShowDialog();
            miCopy_Click(null, null);
        }

        private void miAddBook_Click(object sender, EventArgs e)
        {
            FormAddBook addBook = new FormAddBook();
            addBook.ShowDialog();
            miBook_Click(null, null);
        }
        private void miDelete_Click(object sender, EventArgs e)
        {
            if (dgvLibrarians.CurrentRow != null)
            {
                var sql = "SELECT * FROM sBookWithReader";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    List<int> bookIndex = new List<int>();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        bookIndex.Add(int.Parse(ds.Tables[0].Rows[i][0].ToString()));
                    }

                    int index = int.Parse(dgvLibrarians.Rows[dgvLibrarians.CurrentRow.Index].Cells[0].Value.ToString());
                    if (!bookIndex.Contains(index))
                    {
                        sql = "SELECT * FROM BookActTable";
                        adapter = new SqlDataAdapter(sql, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);

                        List<int> book = new List<int>();
                        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                        {
                            book.Add(int.Parse(ds.Tables[0].Rows[i][0].ToString()));
                        }

                        sql = "SELECT * FROM CopyTable WHERE BookId = " + index;
                        adapter = new SqlDataAdapter(sql, connection);
                        ds = new DataSet();
                        adapter.Fill(ds);

                        if (ds.Tables[0].Rows.Count != 0)
                        {
                            if (!book.Contains(index))
                            {
                                var query = "INSERT INTO BookActTable(Book) " +
                                "VALUES (" + index + ")";
                                SqlCommand command = new SqlCommand(query, connection);
                                command.ExecuteNonQuery();
                            }
                            MessageBox.Show("Книга в акте списания", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("У книги нет экземпляров", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Книга есть на руках у читателя", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            else
            {
                MessageBox.Show("Выберите книгу", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miClose_Click(object sender, EventArgs e)
        {
            if (dgvLibrarians.CurrentRow != null)
            {
                var sql = "SELECT * FROM sReaderWithBook";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);

                    int index = int.Parse(dgvLibrarians.Rows[dgvLibrarians.CurrentRow.Index].Cells[0].Value.ToString());

                    List<int> readerIndex = new List<int>();
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        readerIndex.Add(int.Parse(ds.Tables[0].Rows[i][0].ToString()));
                    }

                    if (!readerIndex.Contains(index))
                    {
                        if (MessageBox.Show("Вы уверены, что хотите Удалить запись?", "Внимание!",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        {
                            var query = "SELECT UserId FROM dbo.ReaderTable " +
                                "WHERE ReaderId = " + index;

                            adapter = new SqlDataAdapter(query, connection);
                            ds = new DataSet();
                            adapter.Fill(ds);

                            query = "DELETE FROM dbo.ReaderTable " +
                                "WHERE ReaderId = " + index;
                            SqlCommand sqlCommand = new SqlCommand(query, connection);
                            sqlCommand.ExecuteNonQuery();

                            query = "DELETE FROM dbo.UserTable " +
                                "WHERE UserId = " + int.Parse(ds.Tables[0].Rows[0][0].ToString());
                            sqlCommand = new SqlCommand(query, connection);
                            sqlCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        MessageBox.Show("У читателя есть книги на руках", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                miReaders_Click(null, null);
            }
            else
            {
                MessageBox.Show("Выберите читателя", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void miAct_Click(object sender, EventArgs e)
        {
            FormRemove remove = new FormRemove();
            remove.btnCancel.Visible = false;
            remove.btnOk.Visible = false;
            remove.ShowDialog();
        }
    }
}
