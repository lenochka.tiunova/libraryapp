﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormAdmin : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormAdmin()
        {
            InitializeComponent();
        }

        string table = "";

        private void miExit_Click(object sender, EventArgs e)
        {
            System.Windows.Forms.Application.Restart();
        }

        private void miBook_Click(object sender, EventArgs e)
        {
            table = "book";
            pnlBooks.Visible = true;
            pnlDeptor.Visible = false;
            string sql = "SELECT * FROM sBooks";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvAdmin.DataSource = ds.Tables[0];
            }
            dgvBookEdit();
        }

        public void dgvBookEdit()
        {
            dgvAdmin.Columns[0].HeaderText = "Номер книги";
            dgvAdmin.Columns[1].HeaderText = "Название книги";
            dgvAdmin.Columns[2].HeaderText = "Издательство";
            dgvAdmin.Columns[3].HeaderText = "Город";
            dgvAdmin.Columns[4].HeaderText = "Год издания";
            dgvAdmin.Columns[5].HeaderText = "Количество страниц";
            dgvAdmin.Columns[6].HeaderText = "Автор";
            dgvAdmin.Columns[7].HeaderText = "  ";
            dgvAdmin.Columns[8].HeaderText = " ";
            dgvAdmin.Columns[9].HeaderText = "Стоимость";
            dgvAdmin.Columns[9].DefaultCellStyle.Format = "N2";
            dgvAdmin.AutoResizeColumns();
            dgvAdmin.AutoResizeRows();
        }

        private void miDeptor_Click(object sender, EventArgs e)
        {
            table = "deptor";
            pnlBooks.Visible = false;
            pnlDeptor.Visible = true;
            string sql = "SELECT * FROM sDeptor";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvAdmin.DataSource = ds.Tables[0];
            }
            dgvAdmin.Columns[0].Visible = false;
            dgvAdmin.Columns[1].HeaderText = "Фамилия";
            dgvAdmin.Columns[2].HeaderText = "Имя";
            dgvAdmin.Columns[3].HeaderText = "Отчество";
            dgvAdmin.Columns[4].HeaderText = "Дата рождения";
            dgvAdmin.Columns[5].HeaderText = "Адрес";
            dgvAdmin.Columns[6].HeaderText = "Рабочий телефон";
            dgvAdmin.Columns[7].HeaderText = "Домашний телефон";
            dgvAdmin.AutoResizeColumns();
        }

        private void FormAdmin_Load(object sender, EventArgs e)
        {
            miBook_Click(null, null);
            cbFilter.SelectedIndex = 0;
        }

        private void dgvAdmin_CurrentCellChanged(object sender, EventArgs e)
        {
            if (dgvAdmin.CurrentRow != null && table == "deptor" && dgvAdmin[0,dgvAdmin.CurrentRow.Index].Value.ToString() != "")
            {
                tbDeptorBooks.Clear();
                int index = int.Parse(dgvAdmin[0, dgvAdmin.CurrentRow.Index].Value.ToString());
                string sql = "SELECT * FROM sBooksDeptor WHERE ReaderId = " + index;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    int i = 1;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        tbDeptorBooks.Text += i + ". " + dr["BookName"].ToString().Trim() + " (Код экземпляра: " + dr["CopyId"] + ")" + "\r\n";
                        i++;
                    }
                }
            }
        }

        private void FormAdmin_FormClosed(object sender, FormClosedEventArgs e)
        {
            System.Windows.Forms.Application.Exit();
        }

        private void dgvAdmin_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (DBNull.Value == e.Value)
            {
                e.Value = "-";
            }
        }

        public void Filter()
        {
            string sql;

            if (cbFilter.SelectedIndex != 0 && cbFilter.SelectedIndex != -1)
            {
                if (cbFilter.SelectedIndex == 1)
                {
                    sql = "SELECT * FROM sBooksNotPop WHERE BookName LIKE '%" + tbSearch.Text + "%'";
                }
                else
                {
                    sql = "SELECT * FROM sBooksPop WHERE BookName LIKE '%" + tbSearch.Text + "%'";
                }
            }
            else
            {
                sql = "SELECT * FROM sBooks WHERE BookName LIKE '%" + tbSearch.Text + "%'";
            }

            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                dgvAdmin.DataSource = ds.Tables[0];
            }
            dgvBookEdit();
        }

        private void cbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void tbSearch_TextChanged(object sender, EventArgs e)
        {
            Filter();
        }

        private void miAct_Click(object sender, EventArgs e)
        {
            FormRemove remove = new FormRemove();
            remove.ShowDialog();
            if (remove.DialogResult == DialogResult.OK)
            {
                miBook_Click(null, null);
            }
        }
    }
}
