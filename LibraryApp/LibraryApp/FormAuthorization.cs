﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormAuthorization : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormAuthorization()
        {
            InitializeComponent();
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            bool login = false;
            string sql = "SELECT * FROM sUser";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i].ItemArray[1].ToString().Trim().Replace("\r\n","") == tbLogin.Text)
                    {
                        login = true;
                        string password = ds.Tables[0].Rows[i].ItemArray[2].ToString().Trim().Replace("\r\n", "");

                        if (password == tbPassword.Text)
                        {
                            this.ShowInTaskbar = false;
                            this.Visible = false;
                            MessageBox.Show("Вы вошли в систему", "Информация", MessageBoxButtons.OK);
                            if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Администратор")
                            {
                                FormAdmin admin = new FormAdmin();
                                admin.ShowDialog();
                            }
                            else if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Библиотекарь")
                            {
                                FormLibrarians librarians = new FormLibrarians();
                                librarians.ShowDialog();
                            }
                            else if (ds.Tables[0].Rows[i].ItemArray[3].ToString() == "Читатель")
                            {
                                FormReaders readers = new FormReaders();
                                readers.ShowDialog();
                            }
                        }
                        else
                        {
                            MessageBox.Show("Неверный пароль", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                        break;
                    }
                }
                if (!login)
                {
                    MessageBox.Show("Пользователь не существует", "Внимание", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }
    }
}
