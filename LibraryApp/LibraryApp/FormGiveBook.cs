﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormGiveBook : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormGiveBook()
        {
            InitializeComponent();
        }

        public int[] book;
        public int[] reader;

        private void FormGiveBook_Load(object sender, EventArgs e)
        {
            var sqlBook = "SELECT * FROM sBookFree";
            var sqlReader = "SELECT DISTINCT LibraryCardId, Surname, Name, Middle FROM sLiner";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlBook, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable readerTable = ds.Tables.Add("Reader");
                adapter = new SqlDataAdapter(sqlReader, connection);
                adapter.Fill(ds, "Reader");
                book = new int[ds.Tables[0].Rows.Count];
                reader = new int[ds.Tables[1].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    book[i] = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                    cbBook.Items.Add(ds.Tables[0].Rows[i][1] + " (" + ds.Tables[0].Rows[i][0] + ")");
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    reader[i] = Convert.ToInt32(ds.Tables[1].Rows[i][0].ToString());
                    cbReader.Items.Add(ds.Tables[1].Rows[i][1] + " " + ds.Tables[1].Rows[i][2] + " " + ds.Tables[1].Rows[i][3] +
                        " (" + ds.Tables[1].Rows[i][0] + ")");
                }
            }
        }

        private void cbBook_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void cbReader_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btnGive_Click(object sender, EventArgs e)
        {
            int count;
            var countBook = "SELECT * FROM sLiner WHERE DateReturn IS NULL AND LibraryCardId = " + reader[cbReader.SelectedIndex];
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(countBook, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                count = ds.Tables[0].Rows.Count;
            }

            if (count < 5)
            {
                int copy;
                var sqlCopy = "SELECT * FROM sCopyFree WHERE BookId = " + book[cbBook.SelectedIndex];
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    SqlDataAdapter adapter = new SqlDataAdapter(sqlCopy, connection);
                    DataSet ds = new DataSet();
                    adapter.Fill(ds);
                    copy = Convert.ToInt32(ds.Tables[0].Rows[0][0].ToString());
                }

                if (!String.IsNullOrEmpty(cbBook.Text) && !String.IsNullOrEmpty(cbReader.Text))
                {
                    var query = "INSERT INTO LinerTable (LibraryCardId, CopyId, DateOfIssue) " +
                        "VALUES (" + reader[cbReader.SelectedIndex] + ", " + copy +
                        ", '" + DateTime.Now.ToShortDateString() + "')";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                    }
                    MessageBox.Show("Данные добавлены в таблицу", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Заполните все поля", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("У данного читателя на руках 5 книг.", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
