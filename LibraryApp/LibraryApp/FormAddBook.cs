﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormAddBook : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormAddBook()
        {
            InitializeComponent();
        }

        private void cbAuthor_CheckedChanged(object sender, EventArgs e)
        {
            if(cbAuthor.Checked)
            {
                gbAuthor.Enabled = false;
            }
            else
            {
                gbAuthor.Enabled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(tbNameBook.Text) && !String.IsNullOrEmpty(cbPublisher.Text)
                && !String.IsNullOrEmpty(cbCity.Text))
            {
                if (cbAuthor.Checked || (!String.IsNullOrEmpty(tbSurname.Text) && !String.IsNullOrEmpty(tbName.Text)
                && !String.IsNullOrEmpty(tbMiddle.Text)))
                {
                    var publisherCity = "SELECT * FROM sPublisherCity WHERE PublisherName = '" + cbPublisher.Text
                        + "' AND City = '" + cbCity.Text + "'";
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(publisherCity, connection);
                        DataSet ds = new DataSet();
                        adapter.Fill(ds);
                        if (ds.Tables[0].Rows.Count == 0)
                        {
                            var publisherSQL = "SELECT * FROM PublisherTable WHERE PublisherName = '" + cbPublisher.Text + "'";
                            adapter = new SqlDataAdapter(publisherSQL, connection);
                            ds = new DataSet();
                            adapter.Fill(ds);

                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                var queryPublisher = "INSERT INTO PublisherTable(PublisherName) " +
                                "VALUES ('" + cbPublisher.Text + "')";
                                SqlCommand command = new SqlCommand(queryPublisher, connection);
                                command.ExecuteNonQuery();
                            }

                            var citySQL = "SELECT * FROM CityTable WHERE City = '" + cbCity.Text + "' ";
                            adapter = new SqlDataAdapter(citySQL, connection);
                            ds = new DataSet();
                            adapter.Fill(ds);

                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                var queryPublisher = "INSERT INTO CityTable(City) " +
                                "VALUES ('" + cbCity.Text + "')";
                                SqlCommand command = new SqlCommand(queryPublisher, connection);
                                command.ExecuteNonQuery();
                            }

                            publisherCity = "INSERT INTO PublisherWithCity(PublisherId, CityId) " +
                            "VALUES ((SELECT PublisherId FROM PublisherTable WHERE PublisherName = @PublisherName), " +
                            "(SELECT CityId FROM CityTable WHERE City = @City))";
                            SqlCommand commandPubSity = new SqlCommand(publisherCity, connection);
                            SqlParameter publisherParameter = new SqlParameter("@PublisherName", cbPublisher.Text);
                            commandPubSity.Parameters.Add(publisherParameter);
                            SqlParameter cityParameter = new SqlParameter("@City", cbCity.Text);
                            commandPubSity.Parameters.Add(cityParameter);
                            commandPubSity.ExecuteNonQuery();
                        }
                    }

                    publisherCity = "SELECT * FROM sPublisherCity WHERE PublisherName = '" + cbPublisher.Text
                        + "' AND City = '" + cbCity.Text + "'";
                    DataSet dsPubCity = new DataSet();

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlDataAdapter adapter = new SqlDataAdapter(publisherCity, connection);
                        adapter.Fill(dsPubCity);
                    }

                    var query = "INSERT INTO BookTable(BookName, PublisherWithCityId, YearPublishing, CountPages, BookCost) " +
                        "VALUES ('" + tbNameBook.Text + "'," + dsPubCity.Tables[0].Rows[0][0] + ", " + nudYear.Value +
                        ", " + nudCount.Value + ", " + nudPrice.Value + ")";

                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(query, connection);
                        command.ExecuteNonQuery();
                    }

                    if (!cbAuthor.Checked)
                    {
                        var author = "SELECT * FROM AuthorTable WHERE SurnameAuthor = '" + tbSurname.Text
                        + "' AND NameAuthor = '" + tbName.Text + "' AND MiddleAuthor = '" + tbMiddle.Text + "'";
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            connection.Open();
                            SqlDataAdapter adapter = new SqlDataAdapter(author, connection);
                            DataSet ds = new DataSet();
                            adapter.Fill(ds);
                            if (ds.Tables[0].Rows.Count == 0)
                            {
                                var addAuthor = "INSERT INTO AuthorTable(SurnameAuthor, NameAuthor, MiddleAuthor) " +
                                "VALUES ('" + tbSurname.Text + "', '" + tbName.Text + "', '" + tbMiddle.Text + "')";
                                SqlCommand commandAdd = new SqlCommand(addAuthor, connection);
                                commandAdd.ExecuteNonQuery();
                            }

                            var authorBook = "INSERT INTO BookWintAuthor (BookId, AuthorId) " +
                                "VALUES ((SELECT BookId FROM BookTable WHERE BookName = @BookName), " +
                                "(SELECT AuthorId FROM AuthorTable WHERE SurnameAuthor = @SurnameAuthor AND NameAuthor = @NameAuthor" +
                                " AND MiddleAuthor = @MiddleAuthor))";
                            SqlCommand command = new SqlCommand(authorBook, connection);
                            SqlParameter bookParameter = new SqlParameter("@BookName", tbNameBook.Text);
                            command.Parameters.Add(bookParameter);
                            SqlParameter surnameParameter = new SqlParameter("@SurnameAuthor", tbSurname.Text);
                            command.Parameters.Add(surnameParameter);
                            SqlParameter nameParameter = new SqlParameter("@NameAuthor", tbName.Text);
                            command.Parameters.Add(nameParameter);
                            SqlParameter middleParameter = new SqlParameter("@MiddleAuthor", tbMiddle.Text);
                            command.Parameters.Add(middleParameter);
                            command.ExecuteNonQuery();
                        }
                    }

                    MessageBox.Show("Данные добавлены в таблицу", "Внимание",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("Заполните все поля", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            else
            {
                MessageBox.Show("Заполните все поля", "Внимание",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void FormAddBook_Load(object sender, EventArgs e)
        {
            nudYear.Maximum = DateTime.Now.Year;
            var sqlPublisher = "SELECT * FROM PublisherTable";
            var sqlCity = "SELECT * FROM CityTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlPublisher, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                DataTable cityTable = ds.Tables.Add("City");
                adapter = new SqlDataAdapter(sqlCity, connection);
                adapter.Fill(ds, "City");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    cbPublisher.Items.Add(ds.Tables[0].Rows[i][1]);
                }
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    cbCity.Items.Add(ds.Tables[1].Rows[i][1]);
                }
            }
        }
    }
}
