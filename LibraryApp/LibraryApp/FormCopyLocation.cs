﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LibraryApp
{
    public partial class FormCopyLocation : Form
    {
        private readonly string connectionString = @"Data Source= DESKTOP-PPCBH42\SQLEXPREESS;
        Initial Catalog = dbLibrary;Integrated Security=True";
        public FormCopyLocation()
        {
            InitializeComponent();
        }

        private void tbLocation_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
            if (Char.IsDigit(e.KeyChar))
            {
                if(tbLocation.Text[0].ToString() == " ")
                {
                    tbLocation.Text = e.KeyChar + ", , ";
                } 
                else if (tbLocation.Text[2].ToString() == " ")
                {
                    tbLocation.Text = tbLocation.Text.Substring(0, 2) + e.KeyChar + ", ";
                }
                else if(tbLocation.Text[4].ToString() == " ")
                {
                    tbLocation.Text = tbLocation.Text.Substring(0, 4) + e.KeyChar;
                }
                
                tbLocation.SelectionStart = tbLocation.Text.Length;
            } 
            else if(e.KeyChar == 8)
            {
                if (tbLocation.Text[4].ToString() != " ")
                {
                    tbLocation.Text = tbLocation.Text.Substring(0, 4) + " ";
                } 
                else if (tbLocation.Text[2].ToString() != " ")
                {
                    tbLocation.Text = tbLocation.Text.Substring(0, 2) + " , ";
                } 
                else if (tbLocation.Text[0].ToString() != " ")
                {
                    tbLocation.Text = " , , ";
                }
            }
        }

        int[] book;

        private void FormCopyLocation_Load(object sender, EventArgs e)
        {
            var sqlBook = "SELECT BookId, BookName FROM BookTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlBook, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                book = new int[ds.Tables[0].Rows.Count];
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    book[i] = Convert.ToInt32(ds.Tables[0].Rows[i][0].ToString());
                    cbNameBooks.Items.Add(ds.Tables[0].Rows[i][1] + " (" + ds.Tables[0].Rows[i][0] + ")");
                }
            }
        }

        private void btnMove_Click(object sender, EventArgs e)
        {
            var sqlLocation = "SELECT Location FROM CopyTable";
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                SqlDataAdapter adapter = new SqlDataAdapter(sqlLocation, connection);
                DataSet ds = new DataSet();
                adapter.Fill(ds);
                List<string> location = new List<string>();
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    location.Add(ds.Tables[0].Rows[i][0].ToString());
                }
                if (!location.Contains(tbLocation.Text))
                {
                    var query = "INSERT INTO CopyTable (BookId, Location) " +
                        "VALUES (" + book[cbNameBooks.SelectedIndex] + ", '" + tbLocation.Text + "')";

                    SqlCommand command = new SqlCommand(query, connection);
                    command.ExecuteNonQuery();

                    MessageBox.Show("Книга размещена", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    this.Close();
                }
                else
                {
                    MessageBox.Show("На этом месте стоит другая книга", "Внимание",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
    }
}
