USE [master]
GO
/****** Object:  Database [dbLibrary]    Script Date: 17.04.2021 21:03:16 ******/
CREATE DATABASE [dbLibrary]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'dbLibrary', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbLibrary.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'dbLibrary_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPREESS\MSSQL\DATA\dbLibrary_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [dbLibrary] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [dbLibrary].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [dbLibrary] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [dbLibrary] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [dbLibrary] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [dbLibrary] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [dbLibrary] SET ARITHABORT OFF 
GO
ALTER DATABASE [dbLibrary] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [dbLibrary] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [dbLibrary] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [dbLibrary] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [dbLibrary] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [dbLibrary] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [dbLibrary] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [dbLibrary] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [dbLibrary] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [dbLibrary] SET  DISABLE_BROKER 
GO
ALTER DATABASE [dbLibrary] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [dbLibrary] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [dbLibrary] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [dbLibrary] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [dbLibrary] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [dbLibrary] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [dbLibrary] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [dbLibrary] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [dbLibrary] SET  MULTI_USER 
GO
ALTER DATABASE [dbLibrary] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [dbLibrary] SET DB_CHAINING OFF 
GO
ALTER DATABASE [dbLibrary] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [dbLibrary] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [dbLibrary] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [dbLibrary] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [dbLibrary] SET QUERY_STORE = OFF
GO
USE [dbLibrary]
GO
/****** Object:  Table [dbo].[LinerTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LinerTable](
	[LinerId] [int] IDENTITY(1,1) NOT NULL,
	[LibraryCardId] [int] NOT NULL,
	[CopyId] [int] NOT NULL,
	[DateOfIssue] [date] NOT NULL,
	[DateReturn] [date] NULL,
 CONSTRAINT [PK_LinerTable] PRIMARY KEY CLUSTERED 
(
	[LinerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserTable](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Surname] [nvarchar](30) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Middle] [nvarchar](30) NOT NULL,
	[Login] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[RoleId] [int] NOT NULL,
 CONSTRAINT [PK_UserTable] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReaderTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReaderTable](
	[ReaderId] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [int] NOT NULL,
	[Adress] [nvarchar](50) NOT NULL,
	[WorkNumber] [nvarchar](20) NULL,
	[HomeNumber] [nvarchar](20) NULL,
	[DateOfBirth] [date] NOT NULL,
 CONSTRAINT [PK_ReaderTable] PRIMARY KEY CLUSTERED 
(
	[ReaderId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LibraryCardTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LibraryCardTable](
	[LibraryCardId] [int] IDENTITY(1,1) NOT NULL,
	[ReaderId] [int] NOT NULL,
 CONSTRAINT [PK_LibraryCardTable] PRIMARY KEY CLUSTERED 
(
	[LibraryCardId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[sDeptor]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE   VIEW [dbo].[sDeptor]
AS
SELECT DISTINCT ReaderTable.ReaderId, UserTable.Surname, UserTable.Name, UserTable.Middle, 
ReaderTable.DateOfBirth, ReaderTable.Adress, ReaderTable.WorkNumber, ReaderTable.HomeNumber
FROM ReaderTable
JOIN
UserTable
ON ReaderTable.UserId = UserTable.UserId
JOIN
LibraryCardTable
ON ReaderTable.ReaderId = LibraryCardTable.ReaderId
JOIN
LinerTable
ON LibraryCardTable.LibraryCardId = LinerTable.LibraryCardId
WHERE LinerTable.DateReturn IS NULL
AND DATEDIFF(DAY, LinerTable.DateOfIssue, SYSDATETIME()) > 13
GO
/****** Object:  Table [dbo].[CopyTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CopyTable](
	[CopyId] [int] IDENTITY(1,1) NOT NULL,
	[BookId] [int] NOT NULL,
	[Location] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_CopyTable] PRIMARY KEY CLUSTERED 
(
	[CopyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookTable](
	[BookId] [int] IDENTITY(1,1) NOT NULL,
	[BookName] [nvarchar](100) NOT NULL,
	[PublisherWithCityId] [int] NOT NULL,
	[YearPublishing] [int] NOT NULL,
	[CountPages] [int] NOT NULL,
	[BookCost] [money] NOT NULL,
 CONSTRAINT [PK_BookTable] PRIMARY KEY CLUSTERED 
(
	[BookId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[sBookCopy]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBookCopy]
AS
SELECT CopyTable.CopyId, BookTable.BookName AS 'Название книги', 
CopyTable.Location AS 'Место размещения'
FROM CopyTable
JOIN
BookTable
ON CopyTable.BookId = BookTable.BookId
GO
/****** Object:  View [dbo].[sReader]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sReader]
AS
SELECT ReaderTable.ReaderId, UserTable.Surname AS 'Фамилия',
UserTable.Name AS 'Имя', UserTable.Middle AS 'Отчество',
ReaderTable.Adress AS 'Адрес', ReaderTable.DateOfBirth AS 'Дата рождения', 
ReaderTable.WorkNumber AS 'Рабочий номер', ReaderTable.HomeNumber AS 'Домашний номер'
FROM ReaderTable
JOIN
UserTable
ON UserTable.UserId = ReaderTable.UserId
GO
/****** Object:  Table [dbo].[RoleTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleTable](
	[RoleId] [int] IDENTITY(1,1) NOT NULL,
	[NameRole] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_RoleTable] PRIMARY KEY CLUSTERED 
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[sUser]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sUser]
AS
SELECT UserTable.UserId, UserTable.Login, UserTable.Password, RoleTable.NameRole
FROM UserTable
JOIN
RoleTable
ON UserTable.RoleId = RoleTable.RoleId
GO
/****** Object:  View [dbo].[sBooksDeptor]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sBooksDeptor]
as
SELECT ReaderTable.ReaderId, BookTable.BookId, BookTable.BookName
FROM LinerTable
JOIN
CopyTable
ON CopyTable.CopyId = LinerTable.CopyId
JOIN
BookTable
ON CopyTable.BookId = BookTable.BookId
JOIN
LibraryCardTable
ON LibraryCardTable.LibraryCardId = LinerTable.LibraryCardId
JOIN
ReaderTable
ON LibraryCardTable.ReaderId = ReaderTable.ReaderId

WHERE LinerTable.DateReturn IS NULL
AND DATEDIFF(DAY, LinerTable.DateOfIssue, SYSDATETIME()) > 13
GO
/****** Object:  Table [dbo].[PublisherTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublisherTable](
	[PublisherId] [int] IDENTITY(1,1) NOT NULL,
	[PublisherName] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_PublisherTable] PRIMARY KEY CLUSTERED 
(
	[PublisherId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CityTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CityTable](
	[CityId] [int] IDENTITY(1,1) NOT NULL,
	[City] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_CityTable] PRIMARY KEY CLUSTERED 
(
	[CityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PublisherWithCity]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PublisherWithCity](
	[PublisherWithCityId] [int] IDENTITY(1,1) NOT NULL,
	[PublisherId] [int] NOT NULL,
	[CityId] [int] NOT NULL,
 CONSTRAINT [PK_PublisherWithCity] PRIMARY KEY CLUSTERED 
(
	[PublisherWithCityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AuthorTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AuthorTable](
	[AuthorId] [int] IDENTITY(1,1) NOT NULL,
	[SurnameAuthor] [nvarchar](30) NOT NULL,
	[NameAuthor] [nvarchar](30) NOT NULL,
	[MiddleAuthor] [nvarchar](30) NOT NULL,
 CONSTRAINT [PK_AuthorTable] PRIMARY KEY CLUSTERED 
(
	[AuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookWintAuthor]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookWintAuthor](
	[BookWintAuthorId] [int] IDENTITY(1,1) NOT NULL,
	[BookId] [int] NOT NULL,
	[AuthorId] [int] NOT NULL,
 CONSTRAINT [PK_BookWintAuthor] PRIMARY KEY CLUSTERED 
(
	[BookWintAuthorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[sBooks]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[sBooks]
AS
SELECT BookTable.BookId, BookTable.BookName, 
PublisherTable.PublisherName, CityTable.City, BookTable.YearPublishing, BookTable.CountPages, AuthorTable.SurnameAuthor,
AuthorTable.NameAuthor, AuthorTable.MiddleAuthor, BookTable.BookCost
FROM BookTable
JOIN
PublisherWithCity
ON BookTable.PublisherWithCityId = PublisherWithCity.PublisherWithCityId
JOIN
PublisherTable
ON PublisherTable.PublisherId = PublisherWithCity.PublisherId
JOIN
CityTable
ON PublisherWithCity.CityId = CityTable.CityId
LEFT JOIN
BookWintAuthor
ON BookWintAuthor.BookId = BookTable.BookId
LEFT JOIN
AuthorTable
ON AuthorTable.AuthorId = BookWintAuthor.AuthorId
GO
/****** Object:  Table [dbo].[FieldOfKnowledgeTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldOfKnowledgeTable](
	[FieldOfKnowledgeId] [int] IDENTITY(1,1) NOT NULL,
	[NameFieldOfKnowledge] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_FieldOfKnowledgeTable] PRIMARY KEY CLUSTERED 
(
	[FieldOfKnowledgeId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BookWithFieldOfKnowledge]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookWithFieldOfKnowledge](
	[BookFieldId] [int] IDENTITY(1,1) NOT NULL,
	[BookId] [int] NOT NULL,
	[FieldOfKnowledgeId] [int] NOT NULL,
 CONSTRAINT [PK_BookWithFieldOfKnowledge] PRIMARY KEY CLUSTERED 
(
	[BookFieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[sBooksLibr]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sBooksLibr]
AS
SELECT BookTable.BookId, BookTable.BookName, FieldOfKnowledgeTable.NameFieldOfKnowledge,
PublisherTable.PublisherName, CityTable.City, BookTable.YearPublishing, BookTable.CountPages, AuthorTable.SurnameAuthor,
AuthorTable.NameAuthor, AuthorTable.MiddleAuthor, BookTable.BookCost
FROM BookTable
JOIN
PublisherWithCity
ON BookTable.PublisherWithCityId = PublisherWithCity.PublisherWithCityId
JOIN
PublisherTable
ON PublisherTable.PublisherId = PublisherWithCity.PublisherId
JOIN
CityTable
ON PublisherWithCity.CityId = CityTable.CityId
LEFT JOIN
BookWintAuthor
ON BookWintAuthor.BookId = BookTable.BookId
LEFT JOIN
AuthorTable
ON AuthorTable.AuthorId = BookWintAuthor.AuthorId
LEFT JOIN
BookWithFieldOfKnowledge
ON BookWithFieldOfKnowledge.BookId = BookTable.BookId
LEFT JOIN
FieldOfKnowledgeTable
ON BookWithFieldOfKnowledge.FieldOfKnowledgeId = FieldOfKnowledgeTable.FieldOfKnowledgeId
GO
/****** Object:  View [dbo].[sBooksForReaders]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW [dbo].[sBooksForReaders]
AS
SELECT BookTable.BookId, BookTable.BookName AS 'Название', 
PublisherTable.PublisherName AS 'Издательство',
BookTable.YearPublishing AS 'Год издания', 
BookTable.CountPages AS 'Количество страниц',
FieldOfKnowledgeTable.NameFieldOfKnowledge,
AuthorTable.SurnameAuthor, 
AuthorTable.NameAuthor, 
AuthorTable.MiddleAuthor
FROM BookTable
LEFT JOIN
BookWintAuthor
ON BookTable.BookId = BookWintAuthor.BookId
LEFT JOIN
AuthorTable
ON BookWintAuthor.AuthorId = AuthorTable.AuthorId
LEFT JOIN
BookWithFieldOfKnowledge
ON BookWithFieldOfKnowledge.BookId = BookTable.BookId
LEFT JOIN
FieldOfKnowledgeTable
ON FieldOfKnowledgeTable.FieldOfKnowledgeId = BookWithFieldOfKnowledge.FieldOfKnowledgeId
JOIN
PublisherWithCity
ON PublisherWithCity.PublisherWithCityId = BookTable.PublisherWithCityId
JOIN
PublisherTable
ON PublisherTable.PublisherId = PublisherWithCity.PublisherId
GO
/****** Object:  View [dbo].[sCopyFree]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sCopyFree]
AS
SELECT DISTINCT CopyTable.CopyId, BookTable.BookId FROM CopyTable

JOIN
BookTable
ON CopyTable.BookId = BookTable.BookId

LEFT JOIN LinerTable
ON CopyTable.CopyId = LinerTable.CopyId

WHERE (NOT EXISTS (
SELECT * FROM LinerTable
WHERE CopyTable.CopyId = LinerTable.CopyId) 
OR NOT EXISTS (SELECT * FROM LinerTable
WHERE CopyTable.CopyId = LinerTable.CopyId AND LinerTable.DateReturn IS NULL))
GO
/****** Object:  View [dbo].[sBooksNotPop]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBooksNotPop]
AS
SELECT DISTINCT BookTable.BookId, BookTable.BookName, 
PublisherTable.PublisherName, CityTable.City, BookTable.YearPublishing, BookTable.CountPages, AuthorTable.SurnameAuthor,
AuthorTable.NameAuthor, AuthorTable.MiddleAuthor, BookTable.BookCost
FROM BookTable
JOIN
PublisherWithCity
ON BookTable.PublisherWithCityId = PublisherWithCity.PublisherWithCityId
JOIN
PublisherTable
ON PublisherTable.PublisherId = PublisherWithCity.PublisherId
JOIN
CityTable
ON PublisherWithCity.CityId = CityTable.CityId
LEFT JOIN
BookWintAuthor
ON BookWintAuthor.BookId = BookTable.BookId
LEFT JOIN
AuthorTable
ON AuthorTable.AuthorId = BookWintAuthor.AuthorId

WHERE (
NOT EXISTS (SELECT * FROM LinerTable
JOIN CopyTable
ON CopyTable.CopyId = LinerTable.CopyId WHERE BookTable.BookId = CopyTable.BookId
AND LinerTable.DateReturn IS NULL))
GO
/****** Object:  View [dbo].[sBooksPop]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBooksPop]
AS
SELECT DISTINCT BookTable.BookId, BookTable.BookName, 
PublisherTable.PublisherName, CityTable.City, BookTable.YearPublishing, BookTable.CountPages, AuthorTable.SurnameAuthor,
AuthorTable.NameAuthor, AuthorTable.MiddleAuthor, BookTable.BookCost
FROM BookTable
JOIN
PublisherWithCity
ON BookTable.PublisherWithCityId = PublisherWithCity.PublisherWithCityId
JOIN
PublisherTable
ON PublisherTable.PublisherId = PublisherWithCity.PublisherId
JOIN
CityTable
ON PublisherWithCity.CityId = CityTable.CityId
LEFT JOIN
BookWintAuthor
ON BookWintAuthor.BookId = BookTable.BookId
LEFT JOIN
AuthorTable
ON AuthorTable.AuthorId = BookWintAuthor.AuthorId
LEFT JOIN
CopyTable
ON BookTable.BookId = CopyTable.BookId

WHERE NOT EXISTS (SELECT * FROM sCopyFree
WHERE sCopyFree.BookId = BookTable.BookId)
GO
/****** Object:  View [dbo].[sBookFree]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBookFree]
AS
SELECT DISTINCT sCopyFree.BookId, BookTable.BookName FROM sCopyFree
JOIN BookTable
ON
BookTable.BookId = sCopyFree.BookId
GO
/****** Object:  View [dbo].[sLiner]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sLiner]
AS
SELECT LinerTable.LinerId, LinerTable.LibraryCardId,
UserTable.Surname, UserTable.[Name], UserTable.Middle,
BookTable.BookId, BookTable.BookName,  
LinerTable.DateOfIssue, LinerTable.DateReturn FROM LinerTable
JOIN CopyTable
ON
LinerTable.CopyId = CopyTable.CopyId
JOIN BookTable
ON
BookTable.BookId = CopyTable.BookId
JOIN LibraryCardTable
ON
LibraryCardTable.LibraryCardId = LinerTable.LibraryCardId
JOIN ReaderTable
ON
LibraryCardTable.ReaderId = ReaderTable.ReaderId
JOIN UserTable
ON
ReaderTable.UserId = UserTable.UserId
GO
/****** Object:  View [dbo].[sBookField]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[sBookField]
AS
SELECT BookWithFieldOfKnowledge.BookFieldId, BookTable.BookId, BookTable.BookName AS 'Название книги', 
FieldOfKnowledgeTable.NameFieldOfKnowledge AS 'Область знаний'
FROM BookWithFieldOfKnowledge
JOIN
BookTable
ON BookWithFieldOfKnowledge.BookId = BookTable.BookId
JOIN
FieldOfKnowledgeTable
ON BookWithFieldOfKnowledge.FieldOfKnowledgeId = FieldOfKnowledgeTable.FieldOfKnowledgeId
GO
/****** Object:  View [dbo].[sPublisherCity]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sPublisherCity]
AS
SELECT PublisherWithCity.PublisherWithCityId, PublisherTable.PublisherId, PublisherTable.PublisherName, 
PublisherWithCity.CityId, CityTable.City FROM PublisherWithCity
JOIN CityTable
ON
PublisherWithCity.CityId = CityTable.CityId
JOIN PublisherTable
ON
PublisherWithCity.PublisherId = PublisherTable.PublisherId
GO
/****** Object:  View [dbo].[sReaderWithBook]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sReaderWithBook]
AS
SELECT DISTINCT ReaderTable.ReaderId FROM LinerTable
JOIN LibraryCardTable
ON
LibraryCardTable.LibraryCardId = LinerTable.LibraryCardId
JOIN ReaderTable
ON
ReaderTable.ReaderId = LibraryCardTable.ReaderId
WHERE DateReturn IS NULL
GO
/****** Object:  View [dbo].[sBookWithReader]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBookWithReader]
AS
SELECT DISTINCT BookTable.BookId FROM LinerTable
JOIN CopyTable
ON LinerTable.CopyId = CopyTable.CopyId
JOIN BookTable
ON CopyTable.BookId = BookTable.BookId
WHERE DateReturn IS NULL
GO
/****** Object:  View [dbo].[sBookRemove]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[sBookRemove]
AS
SELECT BookTable.BookId, BookName, COUNT(CopyTable.BookId) AS CountBook, BookCost, 
COUNT(CopyTable.BookId)*BookCost AS Price FROM BookTable
JOIN CopyTable
ON BookTable.BookId = CopyTable.BookId
group by BookTable.BookId, BookName, BookCost
GO
/****** Object:  Table [dbo].[BookActTable]    Script Date: 17.04.2021 21:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BookActTable](
	[Book] [int] NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[AuthorTable] ON 

INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (1, N'Канель', N'Геннадий', N'Исаакович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (2, N'Головкин', N'Борис', N'Николаевич')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (3, N'Товбин', N'Юрий', N'Константинович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (4, N'Спичак', N'Вячеслав', N'Валентинович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (5, N'Бардаль', N'Анна', N'Борисовна')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (6, N'Денисов', N'Владимир', N'Дмитриевич')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (7, N'Вольфенгаген', N'Вячеслав', N'Эрнестович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (8, N'Трепавлов', N'Владислав', N'Васильевич')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (9, N'Розенкевич', N'Михаил', N'Борисович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (10, N'Перевезенцев', N'Александр', N'Николаевич')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (11, N'Ватульян', N'Александр', N'Ованесович')
INSERT [dbo].[AuthorTable] ([AuthorId], [SurnameAuthor], [NameAuthor], [MiddleAuthor]) VALUES (12, N'Александр', N'Сергеевич', N'Пушкин')
SET IDENTITY_INSERT [dbo].[AuthorTable] OFF
GO
SET IDENTITY_INSERT [dbo].[BookTable] ON 

INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (1, N'	Ударные волны в физике твердого тела', 1, 2018, 300, 300.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (4, N'Электромагнитная томография земных недр', 4, 2015, 200, 200.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (8, N'Транспортный комплекс Дальнего Востока: трансформация и интеграция', 5, 2014, 150, 200.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (9, N'	Петербургский текст Гоголя', 6, 2013, 100, 150.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (10, N'Конструкции языков программирования. Приемы описания', 7, 2012, 150, 215.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (11, N'Этнические элиты в национальной политике России', 8, 2011, 175, 175.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (13, N'Коэффициентные обратные задачи механики', 10, 2009, 234, 345.0000)
INSERT [dbo].[BookTable] ([BookId], [BookName], [PublisherWithCityId], [YearPublishing], [CountPages], [BookCost]) VALUES (19, N'Евгений Онегин', 12, 2015, 129, 100.0000)
SET IDENTITY_INSERT [dbo].[BookTable] OFF
GO
SET IDENTITY_INSERT [dbo].[BookWintAuthor] ON 

INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (1, 1, 1)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (4, 4, 4)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (8, 8, 5)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (9, 9, 6)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (10, 10, 7)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (11, 11, 8)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (14, 13, 11)
INSERT [dbo].[BookWintAuthor] ([BookWintAuthorId], [BookId], [AuthorId]) VALUES (15, 19, 12)
SET IDENTITY_INSERT [dbo].[BookWintAuthor] OFF
GO
SET IDENTITY_INSERT [dbo].[BookWithFieldOfKnowledge] ON 

INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (1, 1, 1)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (4, 4, 4)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (5, 8, 5)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (6, 9, 6)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (7, 10, 7)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (8, 11, 10)
INSERT [dbo].[BookWithFieldOfKnowledge] ([BookFieldId], [BookId], [FieldOfKnowledgeId]) VALUES (10, 13, 9)
SET IDENTITY_INSERT [dbo].[BookWithFieldOfKnowledge] OFF
GO
SET IDENTITY_INSERT [dbo].[CityTable] ON 

INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (1, N'Москва')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (2, N'Санкт-Петербург')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (3, N'Екатеринбург')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (4, N'Новосибирск')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (5, N'Тюмень')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (6, N'Омск')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (7, N'Томск')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (8, N'Краснодар')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (9, N'Сочи')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (10, N'Калининград')
INSERT [dbo].[CityTable] ([CityId], [City]) VALUES (11, N'Орск')
SET IDENTITY_INSERT [dbo].[CityTable] OFF
GO
SET IDENTITY_INSERT [dbo].[CopyTable] ON 

INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (1, 1, N'1,1,1')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (6, 4, N'1,1,6')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (7, 4, N'1,1,7')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (8, 4, N'1,1,8')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (10, 8, N'1,2,1')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (11, 8, N'1,2,2')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (12, 8, N'1,2,3')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (13, 8, N'1,2,4')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (14, 8, N'1,2,5')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (15, 9, N'1,2,6')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (16, 9, N'1,2,7')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (17, 10, N'1,2,8')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (18, 11, N'1,3,1')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (19, 11, N'1,3,2')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (21, 13, N'1,3,4')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (22, 13, N'1,3,5')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (25, 13, N'1,3,6')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (26, 13, N'1,3,7')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (27, 13, N'1,3,8')
INSERT [dbo].[CopyTable] ([CopyId], [BookId], [Location]) VALUES (28, 13, N'1,4,1')
SET IDENTITY_INSERT [dbo].[CopyTable] OFF
GO
SET IDENTITY_INSERT [dbo].[FieldOfKnowledgeTable] ON 

INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (1, N'Физика и астрономия')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (2, N'Биология и медицинские науки')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (3, N'Химия и наука о материалах')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (4, N'Науки о земле')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (5, N'Экономика')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (6, N'Филология и искусствоведение')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (7, N'Информационные системы и базы данных')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (8, N'Инженерные и технические науки')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (9, N'Математика, механика, информатика')
INSERT [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId], [NameFieldOfKnowledge]) VALUES (10, N'Глобальные проблемы и международные отношения')
SET IDENTITY_INSERT [dbo].[FieldOfKnowledgeTable] OFF
GO
SET IDENTITY_INSERT [dbo].[LibraryCardTable] ON 

INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (1, 1)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (2, 2)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (5, 6)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (7, 8)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (8, 9)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (9, 10)
INSERT [dbo].[LibraryCardTable] ([LibraryCardId], [ReaderId]) VALUES (10, 11)
SET IDENTITY_INSERT [dbo].[LibraryCardTable] OFF
GO
SET IDENTITY_INSERT [dbo].[LinerTable] ON 

INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (2, 2, 1, CAST(N'2020-12-20' AS Date), CAST(N'2021-04-12' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (8, 7, 7, CAST(N'2021-02-05' AS Date), CAST(N'2021-02-10' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (9, 8, 8, CAST(N'2021-02-06' AS Date), CAST(N'2021-02-09' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (10, 9, 10, CAST(N'2021-02-12' AS Date), CAST(N'2021-02-15' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (11, 9, 11, CAST(N'2021-03-20' AS Date), CAST(N'2021-04-12' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (12, 10, 18, CAST(N'2021-03-20' AS Date), CAST(N'2021-04-12' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (13, 10, 1, CAST(N'2021-03-20' AS Date), CAST(N'2021-04-17' AS Date))
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (17, 10, 19, CAST(N'2021-04-12' AS Date), NULL)
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (18, 10, 21, CAST(N'2021-04-12' AS Date), NULL)
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (19, 10, 22, CAST(N'2021-04-12' AS Date), NULL)
INSERT [dbo].[LinerTable] ([LinerId], [LibraryCardId], [CopyId], [DateOfIssue], [DateReturn]) VALUES (20, 10, 25, CAST(N'2021-04-12' AS Date), NULL)
SET IDENTITY_INSERT [dbo].[LinerTable] OFF
GO
SET IDENTITY_INSERT [dbo].[PublisherTable] ON 

INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (1, N'АСТ')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (2, N'Эксмо')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (3, N'Наука')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (4, N'Росмэн')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (5, N'Литрес')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (6, N'Европа')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (7, N'Зеленая книга')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (8, N'Весь мир')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (9, N'Текст')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (10, N'Мысль')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (12, N'Клевер')
INSERT [dbo].[PublisherTable] ([PublisherId], [PublisherName]) VALUES (13, N'Клевер1')
SET IDENTITY_INSERT [dbo].[PublisherTable] OFF
GO
SET IDENTITY_INSERT [dbo].[PublisherWithCity] ON 

INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (1, 1, 1)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (2, 2, 2)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (3, 3, 3)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (4, 4, 4)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (5, 5, 5)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (6, 6, 6)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (7, 7, 7)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (8, 8, 8)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (9, 9, 9)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (10, 10, 10)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (11, 13, 11)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (12, 12, 11)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (13, 2, 1)
INSERT [dbo].[PublisherWithCity] ([PublisherWithCityId], [PublisherId], [CityId]) VALUES (14, 3, 5)
SET IDENTITY_INSERT [dbo].[PublisherWithCity] OFF
GO
SET IDENTITY_INSERT [dbo].[ReaderTable] ON 

INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (1, 7, N'Омск, Комарова, 15', N'89513094056', NULL, CAST(N'2000-04-03' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (2, 8, N'Омск, Энтзуиастов, 6', N'89834567234', NULL, CAST(N'1999-01-02' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (6, 11, N'Омск, Тарская, 113', N'89513495678', NULL, CAST(N'2001-11-11' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (8, 13, N'Тюмень, Заозерная, 3', N'89514170519', NULL, CAST(N'1996-11-21' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (9, 14, N'Москва, Забайкальская, 21', N'89836067027', NULL, CAST(N'1990-07-27' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (10, 15, N'Томск, Дианова, 102', N'89834546478', NULL, CAST(N'2002-10-08' AS Date))
INSERT [dbo].[ReaderTable] ([ReaderId], [UserId], [Adress], [WorkNumber], [HomeNumber], [DateOfBirth]) VALUES (11, 16, N'Омск, Химиков, 7', N'89836529459', NULL, CAST(N'2002-12-04' AS Date))
SET IDENTITY_INSERT [dbo].[ReaderTable] OFF
GO
SET IDENTITY_INSERT [dbo].[RoleTable] ON 

INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (1, N'Администратор')
INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (2, N'Библиотекарь')
INSERT [dbo].[RoleTable] ([RoleId], [NameRole]) VALUES (3, N'Читатель')
SET IDENTITY_INSERT [dbo].[RoleTable] OFF
GO
SET IDENTITY_INSERT [dbo].[UserTable] ON 

INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (1, N'Иванов', N'Иван', N'Иванович', N'admin', N'qwerty123', 1)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (2, N'Носков', N'Виталий', N'Георгьевич', N'work', N'123456', 2)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (3, N'Гаврилов', N'Флор', N'Валентинович', N'gFlor', N'pBHxuU', 2)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (4, N'Анисимов', N'Архип', N'Евсеевич', N'aArhip', N'5vHsQe', 2)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (5, N'Соколов', N'Аскольд', N'Святославович', N'bookislive', N'7816os', 2)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (6, N'Самсонов', N'Ипполит', N'Фролович', N'ilovebooks', N'32fkat', 2)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (7, N'Лыткин', N'Вилен', N'Валерьянович', N'iloveread', N'54GGG45', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (8, N'Петров', N'Эдуард', N'Григорьевич', N'bookreader', N'98765', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (11, N'Игнатова', N'Богдана', N'Юрьевна', N'iqxolq', N'4z11x1', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (13, N'Казакова', N'Ирина', N'Федотовна', N'kIrina', N'94m8wx', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (14, N'Юдинцева', N'Анастасия', N'Анатольевна', N'uNastya', N'k4m2io', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (15, N'Красножон', N'Елена', N'Викторовна', N'kElena', N'zzz123', 3)
INSERT [dbo].[UserTable] ([UserId], [Surname], [Name], [Middle], [Login], [Password], [RoleId]) VALUES (16, N'Кадырбекова', N'Надежда', N'Александровна', N'kHope', N'hm33ka', 3)
SET IDENTITY_INSERT [dbo].[UserTable] OFF
GO
ALTER TABLE [dbo].[BookTable]  WITH CHECK ADD  CONSTRAINT [FK_BookTable_PublisherWithCity] FOREIGN KEY([PublisherWithCityId])
REFERENCES [dbo].[PublisherWithCity] ([PublisherWithCityId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookTable] CHECK CONSTRAINT [FK_BookTable_PublisherWithCity]
GO
ALTER TABLE [dbo].[BookWintAuthor]  WITH CHECK ADD  CONSTRAINT [FK_BookWintAuthor_AuthorTable] FOREIGN KEY([AuthorId])
REFERENCES [dbo].[AuthorTable] ([AuthorId])
GO
ALTER TABLE [dbo].[BookWintAuthor] CHECK CONSTRAINT [FK_BookWintAuthor_AuthorTable]
GO
ALTER TABLE [dbo].[BookWintAuthor]  WITH CHECK ADD  CONSTRAINT [FK_BookWintAuthor_BookTable] FOREIGN KEY([BookId])
REFERENCES [dbo].[BookTable] ([BookId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookWintAuthor] CHECK CONSTRAINT [FK_BookWintAuthor_BookTable]
GO
ALTER TABLE [dbo].[BookWithFieldOfKnowledge]  WITH CHECK ADD  CONSTRAINT [FK_BookWithFieldOfKnowledge_BookTable] FOREIGN KEY([BookId])
REFERENCES [dbo].[BookTable] ([BookId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[BookWithFieldOfKnowledge] CHECK CONSTRAINT [FK_BookWithFieldOfKnowledge_BookTable]
GO
ALTER TABLE [dbo].[BookWithFieldOfKnowledge]  WITH CHECK ADD  CONSTRAINT [FK_BookWithFieldOfKnowledge_FieldOfKnowledgeTable] FOREIGN KEY([FieldOfKnowledgeId])
REFERENCES [dbo].[FieldOfKnowledgeTable] ([FieldOfKnowledgeId])
GO
ALTER TABLE [dbo].[BookWithFieldOfKnowledge] CHECK CONSTRAINT [FK_BookWithFieldOfKnowledge_FieldOfKnowledgeTable]
GO
ALTER TABLE [dbo].[CopyTable]  WITH CHECK ADD  CONSTRAINT [FK_CopyTable_BookTable] FOREIGN KEY([BookId])
REFERENCES [dbo].[BookTable] ([BookId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[CopyTable] CHECK CONSTRAINT [FK_CopyTable_BookTable]
GO
ALTER TABLE [dbo].[LibraryCardTable]  WITH CHECK ADD  CONSTRAINT [FK_LibraryCardTable_ReaderTable] FOREIGN KEY([ReaderId])
REFERENCES [dbo].[ReaderTable] ([ReaderId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LibraryCardTable] CHECK CONSTRAINT [FK_LibraryCardTable_ReaderTable]
GO
ALTER TABLE [dbo].[LinerTable]  WITH CHECK ADD  CONSTRAINT [FK_LinerTable_CopyTable] FOREIGN KEY([CopyId])
REFERENCES [dbo].[CopyTable] ([CopyId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LinerTable] CHECK CONSTRAINT [FK_LinerTable_CopyTable]
GO
ALTER TABLE [dbo].[LinerTable]  WITH CHECK ADD  CONSTRAINT [FK_LinerTable_LibraryCardTable] FOREIGN KEY([LibraryCardId])
REFERENCES [dbo].[LibraryCardTable] ([LibraryCardId])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[LinerTable] CHECK CONSTRAINT [FK_LinerTable_LibraryCardTable]
GO
ALTER TABLE [dbo].[PublisherWithCity]  WITH CHECK ADD  CONSTRAINT [FK_PublisherWithCity_CityTable] FOREIGN KEY([CityId])
REFERENCES [dbo].[CityTable] ([CityId])
GO
ALTER TABLE [dbo].[PublisherWithCity] CHECK CONSTRAINT [FK_PublisherWithCity_CityTable]
GO
ALTER TABLE [dbo].[PublisherWithCity]  WITH CHECK ADD  CONSTRAINT [FK_PublisherWithCity_PublisherTable] FOREIGN KEY([PublisherId])
REFERENCES [dbo].[PublisherTable] ([PublisherId])
GO
ALTER TABLE [dbo].[PublisherWithCity] CHECK CONSTRAINT [FK_PublisherWithCity_PublisherTable]
GO
ALTER TABLE [dbo].[ReaderTable]  WITH CHECK ADD  CONSTRAINT [FK_ReaderTable_UserTable] FOREIGN KEY([UserId])
REFERENCES [dbo].[UserTable] ([UserId])
GO
ALTER TABLE [dbo].[ReaderTable] CHECK CONSTRAINT [FK_ReaderTable_UserTable]
GO
ALTER TABLE [dbo].[UserTable]  WITH CHECK ADD  CONSTRAINT [FK_UserTable_RoleTable] FOREIGN KEY([RoleId])
REFERENCES [dbo].[RoleTable] ([RoleId])
GO
ALTER TABLE [dbo].[UserTable] CHECK CONSTRAINT [FK_UserTable_RoleTable]
GO
USE [master]
GO
ALTER DATABASE [dbLibrary] SET  READ_WRITE 
GO
